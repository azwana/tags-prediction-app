import { Component,OnInit} from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import data from '../word_index.json';
import tags from '../tags_index.json';
import {enableProdMode} from '@angular/core';


enableProdMode();
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {

  public quest=' '
  public model: any;
  public questArray: Array<string>
  public vectorQuest
  public outputArray: object;
  
  async ngOnInit(){
 
    this.model = await tf.loadLayersModel('http://localhost:3000/model.json')
   //console.log(this.model.summary());
   }
 
  addQuest(value){  
   if(value!==""){  
    this.quest=value 
    console.log(this.quest)      
    }
    else{
      alert('Field required **')    
    }
  }

  text2Array(quest){
  
    return this.questArray=quest.split(" ") ; 
  }

 tokenWord(questArray){

  var bow =new Array(1000).fill(0)
  var wordVec=new Array
  //console.log(bow)
  wordVec= this.questArray.map((d) => this.getTokenisedWord(d,bow))
  
  for (var i=0;i<bow.length;i++){
      var tae:number =i+1
  
      for (var p in wordVec){
      if (wordVec[p]==tae)
          bow[i+1]=1
    } 
    }  
  //console.log(bow)
  return tf.reshape(bow, [1, 1000])
}

WordVector()
{
  this.vectorQuest=this.tokenWord(this.questArray)
  //console.log(this.vectorQuest)
  this.getPredictWords(this.vectorQuest)
 return this.vectorQuest
  
}
  
 getTokenisedWord(item,vector) {
 
  let id= data[item.toLowerCase()]
  return id
  
}
 /// Loading the model.

 getPredictWords(vectorQuest){

  let y_hat: tf.Tensor
  let yTypeArray: Iterable<unknown>;
 
  var words_ = []
  words_ =this.vectorQuest
  y_hat = this.model.predict(this.vectorQuest)
  y_hat = tf.squeeze(y_hat, [0]);  // reformat tensor dimensions
  yTypeArray = y_hat.dataSync();  // convert tensor to JS TypeArray
  this.outputArray = Array.from(yTypeArray);
  var classTags : Array<string>
  var predictTags
  let outPred=[]
  classTags=tags

  //console.log(this.outputArray)
  for(var k in this.outputArray)
  {
    if(this.outputArray[k]>0.37)
      this.outputArray[k]=1

    else
      this.outputArray[k]=0
  }
 // console.log(this.outputArray)
  for (var i=0;i<100;i++){
    if (this.outputArray[i]==1){
      predictTags=classTags[i]
      outPred.push(predictTags)
    }
  } 
  return outPred
}

}